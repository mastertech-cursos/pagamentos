package br.com.itau.pagamento.services;

import java.util.Random;

import org.springframework.stereotype.Service;

import br.com.itau.pagamento.models.Pagamento;

@Service
public class PagamentoService {
	
	public Boolean efetuarPagamento(Pagamento pagamento)
	{
		// nextInt is normally exclusive of the top value,
	    // so add 1 to make it inclusive
	    //int randomNum = rand.nextInt((max - min) + 1) + min;
		
		Random rand = new Random();
		
		int randomNum = rand.nextInt((100 - 1) + 1) + 1;
		
		if(pagamento!=null && pagamento.getValor()>0 && randomNum >= 30)
		{
			pagamento.setId(randomNum);
			return true;
		}
		
		return false;
	}

}
